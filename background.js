chrome.contextMenus.onClicked.addListener((info, tab) => {

  // get your own api key from https://www.omdbapi.com/apikey.aspx
  let api_key = '513bb092'

  if (info.selectionText) {

    let selection = info.selectionText

    let params = {
      apikey: api_key,
      type:   'movie'
    }

    if (selection.match(/\((\d\d\d\d\))/)) {

      params.y = selection.match(/\((\d\d\d\d)\)/)[1]
      selection = selection.replace( /\(\d\d\d\d\)/, '')
    }

    params.s = selection.trim()

    axios.defaults.timeout = 2000;

    axios.get('http://www.omdbapi.com/', {
      params: params
    })
    .then(response => {

      let data    = response.data,
          message = ''

      if (data.Error) {
        show_message(tab.id, info.frameId, data.Error)
      }
      else {

        if (data.Search && data.Search.length > 0) {

          let get_details = [],
              messages    = []

          data.Search.forEach(result => {

            get_details.push(

              axios.get('http://www.omdbapi.com/', {
                params: {
                  apikey: api_key,
                  i:      result.imdbID
                }
               })
              .then(response => {

                let message =   `<b><a href="https://www.imdb.com/title/${result.imdbID}/" target="_blank">`
                              + `${response.data.Title} (${response.data.Year}, ${response.data.Rated}, ${response.data.Runtime})`
                              + `</a></b><br /><br />${response.data.Plot}<br /><br /><ul>`

                if (response.data.Ratings && response.data.Ratings.length > 0) {

                    message += response.data.Ratings.map(r => `<li>${r.Source}: ${r.Value}</li>`).join('')
                }
                else {

                  message += '<li>No ratings</li>'
                }

                message += '</ul><br />'

                messages.push({
                  year:    response.data.Year,
                  message: message
                })

              })
              .catch(error => {
                messages.push({
                  year:    result.Year,
                  message: `<b>${result.Title}</b><br />${error}<br /><br />`
                })
              })
            )
          })

          axios.all(get_details)
          .then(results => {
            show_message(tab.id, info.frameId, messages)
          })
        }
        else {
          show_message(tab.id, info.frameId, 'No search results')
        }
      }
    })
    .catch(error => {
      show_message(tab.id, info.frameId, 'Search request failed!')
    })
  }
});

function show_message(tab_id, frame_id, messages) {

  // always top level ....?
  frame_id = 0

  message = ''

  if (typeof messages === 'object') {

    message = messages
              .sort((a, b) => {
                 if (a.year > b.year) return -1
                 if (b.year > a.year) return 1
                 return 0
               })
              .map(m => m.message.replace(/'/g, '&quot;'))
              .join('')
  }
  else {

    message = messages
  }

  chrome.tabs.executeScript(tab_id, {
    frameId: frame_id,
    code: `

    if (document.getElementById('imdbify')) {
      document.getElementById('imdbify').innerHTML = '${message}'
    }
    else {
      let div = document.createElement('div')
      div.setAttribute('id', 'imdbify')
      div.style.all = 'initial'
      div.style.position = 'fixed'
      div.style.top = 0
      div.style.left = 0
      div.style.width = '400px'
      div.style.height = '100vh'
      div.style.backgroundColor = '#F0F0F0'
      div.style.fontSize = '14px'
      div.style.padding = '10px'
      div.style.zIndex = '9999'
      div.style.overflow = 'auto'
      div.style.border = '1px solid black'
      div.innerHTML = '${message}'
      div.onclick = function () { document.getElementById('imdbify').remove() }
      document.body.appendChild(div)
    }
`
  })
}

chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id:       'menu',
    title:    'IMDBify',
    contexts: ['selection']
  })
})

